(()=>{
	const navToggle = document.querySelector('.nav-toggle'),
          navLinks = document.querySelectorAll('.navigation ul li a');
	
	navToggle.addEventListener('click', () => {
        document.body.classList.toggle('nav-open');
    });

    navLinks.forEach(link => {
        link.addEventListener('click', () => {
            document.body.classList.remove('nav-open');
        })
    });

    let myForm = document.getElementById('realdevcontact');

    const handleSubmit = (e) => {
    e.preventDefault();

    let formData = new FormData(myForm);

    fetch('/', {
        method: 'POST',
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: new URLSearchParams(formData).toString()
    }).then(() => {
        const formSuccess = document.querySelector(".formSuccess");
        formSuccess.innerHTML = "Form submitted Successfully!"; 
        formSuccess.style.display = "block";
    }).catch((error) =>
        alert(error))
    };

    myForm.addEventListener("submit", handleSubmit);

})();